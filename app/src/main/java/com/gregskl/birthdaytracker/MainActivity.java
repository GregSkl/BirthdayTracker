package com.gregskl.birthdaytracker;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import it.sephiroth.android.library.exif2.ExifInterface;
import it.sephiroth.android.library.exif2.ExifTag;

public class MainActivity extends AppCompatActivity {

    FirebaseAnalytics mFirebaseAnalytics;

    RecyclerView rv;
    public static PersonAdapter adapter;
    SimpleDateFormat formatter;
    List<Person> people;

    private String selectedName;
    private Date selectedDate;
    private UUID selectedId;
    private int selectedIndex;

    File names;
    File folder;

    protected static final int TAKE_PHOTO_ADD = 100;
    protected static final int TAKE_PHOTO_EDIT = 200;
    protected static final int SELECT_PHOTO_ADD = 300;
    protected static final int SELECT_PHOTO_EDIT = 400;

    //TODO check if png images will work
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        formatter = new SimpleDateFormat("dd/MM/yyyy");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        names = new File(getFilesDir(), "names.json");
        folder = new File(getFilesDir(), "images");

        people = new ArrayList<>();
        if(names.exists()) {
            try {
                JSONObject object = new JSONObject(Utils.getStringFromFile(names));
                JSONArray array = (JSONArray) object.get("array");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject temp = array.getJSONObject(i);
                    String name = String.valueOf(temp.get("name"));
                    Date birthday = formatter.parse(String.valueOf(temp.get("birthday")));
                    UUID id = UUID.fromString(String.valueOf(temp.get("id")));
                    people.add(new Person(name, birthday, id));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            new MaterialDialog.Builder(this)
                .title(R.string.welcome)
                .content(Html.fromHtml(getString(R.string.content)))
                .positiveText(android.R.string.ok)
                .show();
        }

        try {
            if (!names.exists())
                names.createNewFile();
            if (!folder.exists())
                folder.mkdirs();
        } catch(IOException e) {
            e.printStackTrace();
        }
        Utils.setAlarms(this, people, false);

        rv = (RecyclerView) findViewById(R.id.cardList);
        adapter = new PersonAdapter(this, people);
        rv.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);

        //Set border between recyclerview items
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                llm.getOrientation());
        rv.addItemDecoration(dividerItemDecoration);

        //Prompt user to add new item
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedName = null;
                selectedDate = null;
                new MaterialDialog.Builder(MainActivity.this)
                    .title(R.string.input_title)
                    .inputRangeRes(1, 25, R.color.colorPrimary)
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                    .input(getString(R.string.input_hint), null, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, final CharSequence input) {
                            Log.d("birthday", String.valueOf(input));
                            final Calendar c = Calendar.getInstance();
                            DatePickerDialog datePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                    c.set(Calendar.YEAR, year);
                                    c.set(Calendar.MONTH, month);
                                    c.set(Calendar.DAY_OF_MONTH, day);
                                    selectedName = String.valueOf(input);
                                    selectedDate = c.getTime();
                                    selectedId = UUID.randomUUID();
                                    /*
                                    new MaterialDialog.Builder(MainActivity.this)
                                            .title(R.string.add_picture_title)
                                            .content(R.string.add_picture_content)
                                            .positiveText(R.string.yes)
                                            .negativeText(R.string.no)
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                                    photoPickerIntent.setType("image/*");
                                                    startActivityForResult(photoPickerIntent, SELECT_PHOTO_ADD);
                                                }
                                            })
                                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    addPerson(selectedName, selectedDate, selectedId);
                                                }
                                            })
                                            .show();*/
                                    new MaterialDialog.Builder(MainActivity.this)
                                        .title(R.string.add_image_title)
                                        .items(R.array.choose_image)
                                        .itemsCallback(new MaterialDialog.ListCallback() {
                                            @Override
                                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                                if(which == 0) {
                                                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                    startActivityForResult(takePicture, TAKE_PHOTO_ADD);
                                                }
                                                else if(which == 1) {
                                                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                                    photoPickerIntent.setType("image/*");
                                                    startActivityForResult(photoPickerIntent, SELECT_PHOTO_ADD);
                                                }
                                                else if(which == 2) {
                                                    addPerson(selectedName, selectedDate, selectedId);
                                                }
                                            }
                                        })
                                        .show();
                                }
                            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                            datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePicker.show();
                        }
                    }).negativeText(android.R.string.cancel).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void addPerson(String name, Date birthday, UUID id) {
        people.add(new Person(name, birthday, id));
        Utils.sort(people);
        adapter.notifyDataSetChanged();
        saveData();
        mFirebaseAnalytics.logEvent("ADD_PERSON", new Bundle());

        Utils.setAlarms(this, people, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case SELECT_PHOTO_ADD:
            case TAKE_PHOTO_ADD:
                if(resultCode == RESULT_OK){
                    Uri si = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    FileOutputStream out = null;
                    try {
                        imageStream = getContentResolver().openInputStream(si);
                        Bitmap image;
                        Bitmap original = Utils.decodeUri(this, si);
                        System.out.println(original);
                        ExifInterface exif = new ExifInterface();
                        exif.readExif(imageStream, ExifInterface.Options.OPTION_ALL);
                        ExifTag tag = exif.getTag(ExifInterface.TAG_ORIENTATION);
                        int orientation = (tag==null ? -1 : tag.getValueAsInt(-1));
                        if(orientation == 8 || orientation == 3 || orientation == 6) {
                            image = Utils.rotateViaMatrix(original, orientation);
                            exif.setTagValue(ExifInterface.TAG_ORIENTATION, 1);
                            exif.removeCompressedThumbnail();
                        }
                        else {
                            image = original;
                        }
                        out = new FileOutputStream(new File(folder, selectedId + ".jpg"));
                        image.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        addPerson(selectedName, selectedDate, selectedId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (imageStream != null)
                                imageStream.close();
                            if (out != null)
                                out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(resultCode == RESULT_CANCELED)
                    addPerson(selectedName, selectedDate, selectedId);
                break;
            case SELECT_PHOTO_EDIT:
            case TAKE_PHOTO_EDIT:
                if(resultCode == RESULT_OK){
                    Uri si = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    FileOutputStream out = null;
                    try {
                        imageStream = getContentResolver().openInputStream(si);
                        Bitmap image;
                        Bitmap original = Utils.decodeUri(this, si);
                        System.out.println(original);
                        ExifInterface exif = new ExifInterface();
                        exif.readExif(imageStream, ExifInterface.Options.OPTION_ALL);
                        ExifTag tag = exif.getTag(ExifInterface.TAG_ORIENTATION);
                        int orientation = (tag==null ? -1 : tag.getValueAsInt(-1));
                        if(orientation == 8 || orientation == 3 || orientation == 6) {
                            image = Utils.rotateViaMatrix(original, orientation);
                            exif.setTagValue(ExifInterface.TAG_ORIENTATION, 1);
                            exif.removeCompressedThumbnail();
                        }
                        else {
                            image = original;
                        }
                        UUID id = people.get(selectedIndex).getId();
                        out = new FileOutputStream(new File(folder, id + ".jpg"));
                        image.compress(Bitmap.CompressFormat.JPEG, 100, out);

                        adapter.notifyDataSetChanged();
                        saveData();
                        imageStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if(imageStream != null)
                                imageStream.close();
                            if(out != null)
                                out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        }
    }

    protected void saveData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PrintWriter clearer = new PrintWriter(names);
                    clearer.print("");
                    clearer.close();
                    JSONObject file = new JSONObject();
                    JSONArray array = new JSONArray();
                    for (final Person person : people) {
                        JSONObject obj = new JSONObject();
                        obj.put("name", person.getName());
                        obj.put("birthday", formatter.format(person.getBirthday()));
                        obj.put("id", person.getId().toString());
                        array.put(obj);
                    }
                    file.put("array", array);
                    FileWriter writer = new FileWriter(names.getAbsoluteFile());
                    writer.write(file.toString());
                    writer.flush();
                    writer.close();
                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void setSelectedIndex(int i) {
        selectedIndex = i;
    }
}
