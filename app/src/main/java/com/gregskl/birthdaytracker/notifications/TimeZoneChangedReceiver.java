package com.gregskl.birthdaytracker.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gregskl.birthdaytracker.MainActivity;
import com.gregskl.birthdaytracker.Person;
import com.gregskl.birthdaytracker.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by magshimim on 12/14/2016.
 */

public class TimeZoneChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //Same code as BootReceiver to load the people list and update the alarms
        if(MainActivity.adapter != null)
            MainActivity.adapter.notifyDataSetChanged();
        
        List<Person> people = new ArrayList<>();
        try {
            File names = new File(context.getFilesDir(), "names.json");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            JSONObject object = new JSONObject(Utils.getStringFromFile(names));
            JSONArray array = (JSONArray) object.get("array");
            for (int i = 0; i < array.length(); i++) {
                JSONObject temp = array.getJSONObject(i);
                String name = String.valueOf(temp.get("name"));
                Date birthday = formatter.parse(String.valueOf(temp.get("birthday")));
                UUID id = UUID.fromString(String.valueOf(temp.get("id")));
                people.add(new Person(name, birthday, id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.setAlarms(context, people, false);
    }
}
