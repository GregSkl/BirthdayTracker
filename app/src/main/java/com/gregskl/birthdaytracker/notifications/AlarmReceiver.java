package com.gregskl.birthdaytracker.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.gregskl.birthdaytracker.BirthdayAnalyzer;
import com.gregskl.birthdaytracker.MainActivity;
import com.gregskl.birthdaytracker.Person;
import com.gregskl.birthdaytracker.Utils;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Gregory on 29-Nov-16.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(MainActivity.adapter != null)
            MainActivity.adapter.notifyDataSetChanged();

        List<Person> people = (List<Person>) intent.getSerializableExtra("people");
        for (Person person : people) {
            BirthdayAnalyzer ba = new BirthdayAnalyzer(person.getBirthday());
            if(ba.hasBirthdayToday()) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_MONTH, -1);
                Utils.addBirthdayNotification(context, person, ba.getNextAgeWithDate(c.getTime()));
            }
        }

        Utils.setAlarms(context, people, true);
    }
}
