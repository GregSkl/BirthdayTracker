package com.gregskl.birthdaytracker;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
/*import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;*/

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by magshimim on 11/20/2016.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder> {

    private MainActivity context;
    private List<Person> people;
    private SimpleDateFormat displayFormatter = (SimpleDateFormat) SimpleDateFormat.getDateInstance(DateFormat.MEDIUM);
    //ImageLoader imageLoader = ImageLoader.getInstance();

    public PersonAdapter(Context context, List<Person> people) {
        this.context = (MainActivity) context;
        this.people = people;
        Utils.sort(people);
        //imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public PersonAdapter.PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.birthday, parent, false);
        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PersonAdapter.PersonViewHolder holder, int position) {
        final Person p = people.get(position);
        holder.title.setText(p.getName());
        holder.birthdate.setText(displayFormatter.format(p.getBirthday()));
        holder.image.setImageResource(R.drawable.emptyprofile);

        /*File file = new File(context.folder, p.getId() + ".jpg");
        if (file.exists()) {
            ImageSize targetSize = new ImageSize(150, 150);
            imageLoader.loadImage("file://" + file.getPath(), targetSize, null, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.image.setImageBitmap(loadedImage);
                }
            });
        }*/

        File file = new File(context.folder, p.getId() + ".jpg");
        if(file.exists())
            Glide.with(context)
                .load(file.getPath())
                .fitCenter()
                .placeholder(R.drawable.emptyprofile)
                .dontAnimate()
                .diskCacheStrategy( DiskCacheStrategy.NONE )
                .skipMemoryCache(true)
                .into(holder.image);

        BirthdayAnalyzer ba = new BirthdayAnalyzer(p.getBirthday());
        int nextAge = ba.getNextAge();
        if(ba.hasBirthdayToday())
            holder.content.setText(Html.fromHtml(context.getString(R.string.has_birthday_text, nextAge - 1, context.getResources().getQuantityString(R.plurals.year, nextAge - 1))));
        else
            holder.content.setText(Html.fromHtml(context.getString(R.string.birthday_text, nextAge, context.getResources().getQuantityString(R.plurals.year, nextAge), ba.getDaysUntilBirthday(), context.getResources().getQuantityString(R.plurals.day, ba.getDaysUntilBirthday()))));
        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                new MaterialDialog.Builder(context)
                    .title(p.getName())
                    .items(R.array.options_array)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if(which == 0) {
                                new MaterialDialog.Builder(context)
                                    .title(R.string.edit_name_title)
                                    .inputRangeRes(1, 25, R.color.colorPrimary)
                                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                                    .input(context.getString(R.string.input_hint), p.getName(), new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(MaterialDialog dialog, CharSequence input) {
                                            p.setName(String.valueOf(input));
                                            notifyDataSetChanged();
                                            context.saveData();
                                            Utils.setAlarms(context, people, false);
                                            context.mFirebaseAnalytics.logEvent("EDIT_NAME", new Bundle());
                                        }
                                    }).negativeText(android.R.string.cancel).show();
                            }
                            else if(which == 1) {
                                final Calendar c = Calendar.getInstance();
                                c.setTime(p.getBirthday());
                                DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                        c.set(Calendar.YEAR, year);
                                        c.set(Calendar.MONTH, month);
                                        c.set(Calendar.DAY_OF_MONTH, day);
                                        p.setBirthday(c.getTime());
                                        Utils.sort(people);
                                        notifyDataSetChanged();
                                        context.saveData();
                                        Utils.setAlarms(context, people, false);
                                        context.mFirebaseAnalytics.logEvent("EDIT_BIRTHDAY", new Bundle(0));
                                    }
                                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                                datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                                datePicker.show();
                            }
                            else if(which == 2) {
                                context.setSelectedIndex(holder.getAdapterPosition());
                                new MaterialDialog.Builder(context)
                                    .title(R.string.edit_image_title)
                                    .items(R.array.edit_image)
                                    .itemsCallback(new MaterialDialog.ListCallback() {
                                        @Override
                                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                            if(which == 0) {
                                                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                context.startActivityForResult(takePicture, MainActivity.TAKE_PHOTO_EDIT);
                                            }
                                            else if(which == 1) {
                                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                                photoPickerIntent.setType("image/*");
                                                context.startActivityForResult(photoPickerIntent, MainActivity.SELECT_PHOTO_EDIT);
                                            }
                                            else if(which == 2) {
                                                notifyDataSetChanged();
                                                context.saveData();
                                                File f = new File(context.folder, p.getId() + ".jpg");
                                                f.delete();
                                                context.mFirebaseAnalytics.logEvent("REMOVE_PICTURE", new Bundle());
                                            }
                                        }
                                    })
                                    .show();
                            }
                            else if(which == 3) {
                                new MaterialDialog.Builder(context)
                                    .content(context.getString(R.string.delete_content, p.getName()))
                                    .positiveText(R.string.delete)
                                    .negativeText(android.R.string.cancel)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            people.remove(p);
                                            notifyDataSetChanged();
                                            context.saveData();
                                            File f = new File(context.folder, p.getId() + ".jpg");
                                            f.delete();
                                            Utils.setAlarms(context, people, false);
                                            context.mFirebaseAnalytics.logEvent("DELETE_PERSON", new Bundle());
                                        }
                                    }).show();
                            }
                        }
                    }).show();
                return true;
            }
        });
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        protected ImageView image;
        protected TextView title;
        protected TextView content;
        protected TextView birthdate;
        protected RelativeLayout layout;

        public PersonViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            title = (TextView) v.findViewById(R.id.title);
            content = (TextView) v.findViewById(R.id.content);
            birthdate = (TextView) v.findViewById(R.id.birthdate);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
        }
    }
}
